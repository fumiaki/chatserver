import ring_buffer
import pprint
import thread
import random
import inspect
from ring_buffer import RingBuffer

class User(object):
    def __init__(self):
        self._done = False
        self._name = None
        self._room = None
        self._index = 0
        self._message = RingBuffer(10)
        self._message_lock = thread.allocate_lock() # this is the only value accessed across threads


    def is_done(self):
        return self._done

    def set_done(self):
        self._done = True

    def get_name(self):
        return self._name

    def set_name(self, name):
        self._name = name

    def get_index(self):
        return self._index

    def set_index(self, new_index):
        self._index = new_index

    def incr_index(self):
        self._index += 1

    def get_room(self):
        return self._room

    def set_room(self, new_room):
        self._room = new_room

    def add_message(self, message):
        with self._message_lock:
            self._message.append((None, message))

    def add_private_message(self, sender, message):
        with self._message_lock:
            self._message.append((sender.get_name(), message))

    def has_message(self):
        with self._message_lock:
            return not self._message.empty()

    def pop_messages(self):
        with self._message_lock:
            count = self._message.get_count()
            result = []
            for i in range(0, count):
                data = self._message.get(i)
                if not data[0]:
                    result.append(data[1])
                else:
                    result.append(data[0]+'> '+data[1])
            self._message.clear()
            return result

class ChatRoom(object):
    def __init__(self):
        self._lines = ring_buffer.RingBuffer(128)
        self._users = {}
        self._lock = thread.allocate_lock()

    def join(self, user):
        with self._lock:
            for other_user in self._users.values():
                other_user.add_message('* new user joined chat: '+user.get_name()+'\n')
            self._users[user.get_name()] = user

    def leave(self, user):
        with self._lock:
            del self._users[user.get_name()]
            for user in self._users.values():
                user.add_message('* user has left chat: '+user.get_name()+'\n')

    def chat(self, user, line):
        with self._lock:
            self._lines.append((user.get_name(), line))

    def has(self, index):
        with self._lock:
            return self._lines.has(index)

    def get(self, index):
        with self._lock:
            return self._lines.get(index)

    def get_end(self):
        with self._lock:
            return self._lines.get_end()

    def members(self):
        with self._lock:
            return self._users
    
    def get_member_num(self):
        with self._lock:
            return len(self._users)

    def get_member(self, name):
        return self._users[name]

    def message(self, line):
        with self._lock:
            self._lines.append((None, line))

def command(arg_num=0):
    def command_wrapper(func):
        func.arg_num = arg_num
        func.command = True
        return func
    return command_wrapper

class Chat(object):
    def __init__(self):
        self._rooms = {}
        self._all_users = {}
        self._lock = thread.allocate_lock()

    def login(self, user_name):
        user = User()
        user.set_name(user_name)
        self._all_users[user_name] = user
        return 'Welcome '+user.get_name()+'\nUse /help to see list of commands\n'

    def get_user(self, user_name):
        return self._all_users[user_name]

    def parse_and_handle(self, user, line):
        tokens = line.split()
        if tokens and 0<tokens[0]:
            if not user.get_name():
                if not tokens[0] in self._all_users:
                    user.set_name(tokens[0])
                    self._all_users[tokens[0]] = user
                    return 'Welcome '+user.get_name()+'\nUse /help to see list of commands\n'
                else:
                    return 'Sorry, name taken\n'
            elif '/'==tokens[0][0]:
                command = tokens[0][1:]
                if hasattr(self.__class__, command):
                    if not self.verify_arguments(command, tokens):
                        return self.bad_arguments(command)
                    return getattr(self.__class__, command)(self, user, tokens)
                else:
                    return 'no such command\n'
            else:
                self.chat(user, line)
        return ''

    def verify_arguments(self, command, tokens):
        '''
        If arg_num is set to the method, then check, otherwise return True and allow.
        '''
        method = getattr(self, command)
        if hasattr(method, 'arg_num'):
            arg_num = getattr(method, 'arg_num')
            return len(tokens)>=1+arg_num
        else:
            return True

    def bad_arguments(self, command):
        method = getattr(self.__class__, command)
        print repr(method)
        arg_num = getattr(method, 'arg_num')
        if 1==arg_num:
            return 'error: /{0} requires 1 argument\n'.format(command)
        else:
            return 'error: /{0} requires {1} arguments\n'.format(command, arg_num)

    def remove_user(self, user):
        del self._all_users[user.get_name()]

    @command(1)
    def create(self, user, tokens):
        """create a new chat room"""
        with self._lock:
            try:
                if not tokens[1] in self._rooms:
                    self._rooms[tokens[1]] = ChatRoom()
                    return 'created room '+tokens[1]+'\n'
                else:
                    return 'room '+tokens[1]+' already exists\n'
            except Exception as e:
                return 'no name entered\n'

    @command()
    def rooms(self, user, tokens):
        """see list of rooms and number of users in each room"""
        if 0<len(self._rooms):
            result = 'Active rooms are\n'
            for room_name, room in self._rooms.items():
                result += '* '+room_name+ '('+str(room.get_member_num())+')\n'
            result += 'end of list.\n'
            return result
        else:
            return 'no rooms\n'

    @command(1)
    def join(self, user, tokens):
        """join a chat room"""
        try:
            if 1>len(tokens):
                return 'no name entered\n'
            elif not tokens[1] in self._rooms:
                return 'no such room found\n'
            self._rooms[tokens[1]].join(user)
            user.set_room(tokens[1])
            user.set_index(self._rooms[tokens[1]].get_end())
            result = 'entering room: '+tokens[1]+'\n'
            result += self.members(user, None)
            return result
        except Exception as e:
            pprint.pprint(e)
            raise

    @command()
    def leave(self, user, tokens):
        """leave a chat room"""
        try:
            self._rooms[user.get_room()].leave(user)
            user.set_room(None)
            return '* user has left chat: '+user.get_name()+' (** this is you)\n'
        except Exception as e:
            return 'not in room\n'

    @command()
    def members(self, user, tokens):
        """see list of members in current room, or in a room specified"""
        room_name = user.get_room()
        if tokens and 1<len(tokens):
            room_name = tokens[1]
        if room_name in self._rooms:
            members = self._rooms[room_name].members()
            result = ''
            for member_name in members.keys():
                if member_name==user.get_name():
                    result += '* '+member_name + '(** this is you)\n'
                else:
                    result += '* '+member_name + '\n'
            return result
        else:
            return 'not in room\n'

    @command(2)
    def message(self, user, tokens):
        """send private message to another user"""
        other_user_name = tokens[1]
        if other_user_name in self._all_users:
            message = ' '.join(tokens[2:])+'\n'
            self._all_users[other_user_name].add_private_message(user, message)
            return '{0} messaged {1}\n'.format(user.get_name(), other_user_name)
        else:
            return 'no such user found\n'

    @command()
    def help(self, user, tokens):
        """see list of commands"""
        result = ''
        members = inspect.getmembers(self.__class__, predicate=inspect.ismethod)
        for name, attribute in members:
            if hasattr(attribute, 'command'):
                result += '/{0} : {1}\n'.format(name, attribute.__doc__)
        return result

    @command()
    def quit(self, user, tokens):
        """quit chat"""
        user.set_done()
        return 'BYE\n'

    def chat(self, user, line):
        try:
            if not user.get_room() in self._rooms:
                return 'not in valid room\n'
            self._rooms[user.get_room()].chat(user, line)
            return line
        except Exception as e:
            pprint.pprint(e)
            raise

    def has_line(self, user):
        try:
            next_line = self._rooms[user.get_room()].has(user.get_index())
            return next_line
        except Exception as e:
            return False

    def get_line(self, user):
        next_line = self._rooms[user.get_room()].get(user.get_index())
        if next_line:
            user.incr_index()
            if next_line[0]:
                return next_line[0]+': '+next_line[1]
            else:
                return '* '+next_line[1]
        else:
            return '\n'

    def update(self, user):
        if user.has_message():
            messages = user.pop_messages()
        else:
            messages = []
        if self.has_line(user):
            messages.append(self.get_line(user))
        return messages


