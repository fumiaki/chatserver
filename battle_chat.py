import random
import chat
import thread

class BattleUser(chat.User):
    def __init__(self):
        super(BattleUser, self).__init__()
        self._health = random.randrange(50)
        self._max_health = self._health
        self._attack = random.randrange(100-self._health)
        self._defense = random.randrange(100-self._health-self._attack)
        self._speed = 100-self._health-self._attack-self._defense
        self._item = {}
        self._battle_lock = thread.allocate_lock() # this is to protect health and battle related stats


    def get_health(self):
        with self._battle_lock:
            return self._health

    def get_max_health(self):
        with self._battle_lock:
            return self._max_health

    def change_health(self, amount):
        with self._battle_lock:
            prev_health = self._health
            self._health = min(self._max_health, self._health+amount)
            return self._health - prev_health # return how much the health actually changed

    def powerup(self, amount):
        with self._battle_lock:
            self._attack += amount
            self._defense += amount
            self._speed += amount

    def is_dead(self):
        with self._battle_lock:
            return 0>=self._health

    def get_attack(self):
        with self._battle_lock:
            return self._attack

    def get_defense(self):
        with self._battle_lock:
            return self._defense

    def get_speed(self):
        with self._battle_lock:
            return self._speed

    def add_item(self, item):
        with self._battle_lock:
            if not item in self._item:
                self._item[item] = 0
            self._item[item] += 1

    def has_item(self, item):
        with self._battle_lock:
            return item in self._item

    def remove_item(self, item):
        with self._battle_lock:
            if 0<self._item[item]:
                self._item[item] -= 1
            else:
                raise Exception('not enough of {0}'.format(item))

    def get_stat(self):
        with self._battle_lock:
            result = 'hp {0}/{1}, attack {2}, defense {3}, speed {4}\nInventory: '.format(self._health, 
                self._max_health, self._attack, self._defense, self._defense)
            had_item = False
            for item, count in self._item.items():
                if 0<count:
                    result += '{0} x{1}\n'.format(item, count)
                    had_item = True
            if not had_item:
                result += 'None\n'
            return result

class BattleRoom(chat.ChatRoom):
    def __init__(self):
        self._item = None
        super(BattleRoom, self).__init__()

    def add_item(self, item_name):
        self._item = item_name

    def get_item(self):
        return self._item

    def remove_item(self):
        self._item = None

class BattleChat(chat.Chat):
    def __init__(self):
        super(BattleChat, self).__init__()
        data = {
            'default':{
                'search':'potion'
            }
        }
        self.read_data(data)

    def read_data(self, data):
        for room_name, room in data.items():
            self.create(None, ['create', room_name, room.get('search', None)])


    def login(self, user_name):
        user = BattleUser()
        user.set_name(user_name)
        self._all_users[user_name] = user
        return 'Welcome '+user.get_name()+'\nUse /help to see list of commands\n'
        
    @chat.command(1)
    def create(self, user, tokens):
        """create a new chat room"""
        with self._lock:
            try:
                if not tokens[1] in self._rooms:
                    self._rooms[tokens[1]] = BattleRoom()
                    if 2<len(tokens) and tokens[2]:
                        self._rooms[tokens[1]].add_item(tokens[2])
                    else:
                        self._rooms[tokens[1]].add_item(self.get_random_item())
                    return 'created room '+tokens[1]+'\n'
                else:
                    return 'room '+tokens[1]+' already exists\n'
            except Exception as e:
                return 'no name entered\n'

    @chat.command()
    def search(self, user, tokens):
        """search current room"""
        with self._lock:
            item = self._rooms[user.get_room()].get_item()
            if item:
                self._rooms[user.get_room()].remove_item()
                user.add_item(item)
                self._rooms[user.get_room()].message('{0} found a {1}\n'.format(user.get_name(), item))
                return 'searched room\n'
            else:
                return 'searched room and found nothing\n'

    @chat.command()
    def stat(self, user, tokens):
        """see stat for self"""
        return user.get_stat()

    @chat.command(1)
    def attack(self, user, tokens):
        """attack another user in the same room"""
        try:
            room_name = user.get_room()
            room = self._rooms[room_name]
            opponent = room.get_member(tokens[1])
            damage = self.calculate_damage(user, opponent)
            opponent.change_health(-damage)
            room.message('{0} did {1} damage to {2}\n'.format(user.get_name(), damage, opponent.get_name()))
            return '{0} attacked {1}\n'.format(user.get_name(), opponent.get_name())
        except Exception as e:
            if not user.get_room():
                return 'not in a room\n'
            else:
                return 'no such user\n'

    @chat.command(1)
    def use(self, user, tokens):
        """use an item that you have"""
        item_name = tokens[1]
        try:
            if user.has_item(item_name):
                item_function = 'item_'+item_name
                if hasattr(self.__class__, item_function):
                    user.remove_item(item_name)
                    return getattr(self.__class__, item_function)(self, user)
                else:
                    return 'no such item\n'
        except Exception as e:
            print repr(e)
            pass
        return 'you do not have that item\n'

    def calculate_damage(self, attacker, victim):
        raw_damage = max(1, attacker.get_attack()-attacker.get_defense())
        return min(raw_damage, victim.get_health())

    def update(self, user):
        if user.is_dead():
            user.set_done()
            room_name = user.get_room()
            room = self._rooms[room_name]
            room.message('{0} has been defeated\n'.format(user.get_name()))
        return super(BattleChat, self).update(user)

    def get_random_item(self):
        items = ['potion', 'powerup', None]
        return random.choice(items)

    def item_potion(self, user):
        amount = user.change_health(30)
        return 'used potion to heal {0}\n'.format(amount)

    def item_powerup(self, user):
        amount = random.randrange(1, 3)
        user.powerup(amount)
        return 'used powerup to increase base stats by {0}\n'.format(amount)

