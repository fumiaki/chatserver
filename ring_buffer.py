class RingElement:
    def __init__(self, data):
        self._next = None
        self._prev = None
        self._payload = data

    def get_data(self):
        return self._payload

class RingBuffer:
    def __init__(self, max_size):
        self._lines = [None]*max_size # create an empty list of max size
        self._start = 0
        self._end = 0
        self._max = max_size

    def append(self, data):
        self._lines[self._end%self._max] = RingElement(data)
        self._end += 1
        if self._end-self._start>self._max:
            self._start = self._end-self._max

    def has(self, index):
        return index<self._end

    def get(self, index):
        actual_index = (self._start + index) % self._max
        try:
            return self._lines[actual_index].get_data()
        except Exception as e:
            return None

    def get_count(self):
        return self._end - self._start

    def get_end(self):
        return self._end

    def empty(self):
        return self._start==self._end

    def clear(self):
        self._start = self._end = 0
        self._lines = []