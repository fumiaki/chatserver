# README #


### How do I get set up? ###
The server uses python 2.7 and a few python packages.
Following are the steps to get the server running on OS X.

* Get the latest code from the repository (here!).
* Setup your virtual environment.
* * Go to the directory where the code resides
* * Use 'virtualenv' to create your virtual environment

```
virtualenv venv

```
* * Here is a good tutorial if stuck [[https://hackercodex.com/guide/python-development-environment-on-mac-osx/]]
* Start your virtual environment

```
source venv/bin/activate
```

* Install necessary packages
* * Install gevent, which is a threading model and network server

```
pip install gevent

```
* * Install flask, which is a http server microframework

```
pip install flask
```
* Run the server 
```
python main.py
```


### Notes ###
* The same server is already running at 52.90.198.228.
* Port 5000 is listening for a telnet based protocol
* Port 5080 is listening for REST based protocol used by the Unity client
* Be sure to connect to the right port!


### Development Notes ###
* [Link to Client](https://bitbucket.org/fumiaki/chatclient)
* Future work could include
* * User persistence and authentication
* * Chat log persistence, allowing for multiple servers
* * Admin tools