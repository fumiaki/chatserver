from __future__ import absolute_import
import pprint
from flask import Flask, render_template, g, request

app = Flask(__name__)

@app.route('/')
def index():
    res = render_template('test.html')
    return res
  #return 'Hello World'

@app.route('/login', methods=['POST'])
def login():
    result = g.chat.login(request.form['user_name'])
    return result

@app.route('/logout', methods=['POST'])
def logout():
    user = _get_user(request.form['user_name'])
    result = g.chat.remove_user(user.get_name())
    return result


@app.route('/create', methods=['POST'])
def create():
    try:
        room_name = request.form['room_name']
        user = _get_user(request.form['user_name'])
        result = g.chat.create(user, ['create', room_name])
        return result
    except:
        return 'not logged in\n'

@app.route('/rooms', methods=['GET'])
def rooms():
    try:
        print request.args.get('user_name')
        user = _get_user(request.args.get('user_name'))
        result = g.chat.rooms(user, ['rooms'])
        return result
    except:
        return 'not logged\n'

@app.route('/join', methods=['POST'])
def join():
    try:
        room_name = request.form['room_name']
        user = _get_user(request.form['user_name'])
        result = g.chat.join(user, ['join', room_name])
        return result
    except:
        return 'not logged\n'

@app.route('/leave', methods=['POST'])
def leave():
    try:
        user = _get_user(request.form['user_name'])
        result = g.chat.leave(user, ['leave'])
        return result
    except:
        return 'not logged\n'

@app.route('/chat', methods=['POST'])
def chat():
    try:
        user = _get_user(request.form['user_name'])
        message = request.form['message']
        result = g.chat.chat(user, message)
        return result
    except:
        return 'not logged\n'

@app.route('/poll', methods=['POST'])
def poll():
    try:
        user = _get_user(request.form['user_name'])
        messages = g.chat.update(user)
        result = ''
        for message in messages:
            result += message+'\n'
        return result
    except Exception as e:
        pprint.pprint(e)
        return 'not logged\n'


def _get_user(user_name):
    return g.chat.get_user(user_name)