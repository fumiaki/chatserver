from __future__ import absolute_import
from gevent.server import StreamServer
from gevent import monkey
monkey.patch_all()
from flask import Flask, render_template, g
from gevent import wsgi
import threading
from chat import Chat, User
import select
from battle_chat import BattleChat, BattleUser
import chat_app

chat = BattleChat()

def echo(socket, address):
    try:
        print('New connection from %s:%s' % address)
        socket.sendall(b'Welcome to the GungHo test chat server.\r\n')
        socket.sendall(b'Login Name?\r\n')
        # using a makefile because we want to use readline()
        rfileobj = socket.makefile(mode='rb')
        current_user = BattleUser()
        while not current_user.is_done():
            ready_to_read, ready_to_write, in_error = \
                   select.select(
                      [socket],
                      [],
                      [],
                      1)
            if 0<len(ready_to_read):
                line = rfileobj.readline()
                if not line:
                    print("client disconnected")
                    break
                if current_user.get_name():
                    print current_user.get_name()+'=> '+line
                else:
                    print 'new user=> '+line
                result = chat.parse_and_handle(current_user, line)
                socket.sendall(result)
            messages = chat.update(current_user)
            for message in messages:
                socket.sendall(message)
    finally:
        # This should ensure that the username does not remain taken even if there's a bug in the server
        chat.remove_user(current_user)
        rfileobj.close()


@chat_app.app.before_request
def before_request():
    g.chat = chat

# uncomment below to use the flask's debugging features
#chat_app.app.debug = True
#chat_app.app.run(host='0.0.0.0', port=5080)

# start flask using the gevent framework
wsgi_server = wsgi.WSGIServer(('0.0.0.0', 5080), chat_app.app)
t = threading.Thread(target=wsgi_server.serve_forever)
t.setDaemon(True)
t.start()

# start telnet server on the main thread
server = StreamServer(('0.0.0.0', 5000), echo)
server.serve_forever()
